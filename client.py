import socket

def receive_file():
    with open('received.txt', 'wb') as file:
        while True:
            data = client_socket.recv(1024)
            if not data:
                break
            file.write(data)

def main():
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect(('angsila.informatics.buu.ac.th', 12345))
    
    receive_file()
    
    print("File received and saved as 'received.txt'")
    client_socket.close()

if __name__ == '__main__':
    main()